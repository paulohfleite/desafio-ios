//
//  Shot.swift
//  Dribbble
//
//  Created by Paulo Henrique Leite on 16/11/15.
//  Copyright © 2015 Paulo Henrique Leite. All rights reserved.
//

import Foundation

class Shot {
    
    var id : Int!
    var title : String!
    var date : String!
    var description : String!
    var commentCount : Int!
    var likesCount :  Int!
    var viewsCount : Int!
    var commentUrl : String!
    var imageUrl : String!
    
    var imageData : NSData?
    
    var user: User!
    
    
    init(data : NSDictionary){
        
        self.id = data["id"] as! Int
        self.commentCount = data["comments_count"] as! Int
        self.likesCount = data["likes_count"] as! Int
        self.viewsCount = data["views_count"] as! Int
        
        self.commentUrl = Helper.getStringFromJSON(data, key: "comments_url")
        self.title = Helper.getStringFromJSON(data, key: "title")
        
        let dateInfo = Helper.getStringFromJSON(data, key: "created_at")
        self.date = Helper.formatDate(dateInfo)
        
        let desc = Helper.getStringFromJSON(data, key: "description")
        self.description = Helper.stripHTML(desc)
        
        let images = data["images"] as! NSDictionary
        self.imageUrl = Helper.getStringFromJSON(images, key: "normal")
        
        if let userData = data["user"] as? NSDictionary {
            self.user = User(data: userData)
        }
    }
}
