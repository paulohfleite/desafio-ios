//
//  Comment.swift
//  Dribbble
//
//  Created by Paulo Henrique Leite on 16/11/15.
//  Copyright © 2015 Paulo Henrique Leite. All rights reserved.
//

import Foundation
import UIKit

class Comment {
    
    var id : Int!
    var body : String!
    var date : String!
    
    var user : User!
    
    init(data : NSDictionary){
        
        self.id = data["id"] as! Int
        let bodyHTML = Helper.getStringFromJSON(data, key: "body")
        self.body = Helper.stripHTML(bodyHTML)
        
        let dateInfo = Helper.getStringFromJSON(data, key: "created_at")
        self.date = Helper.formatDate(dateInfo)
 
        if let userData = data["user"] as? NSDictionary {
            self.user = User(data: userData)
        }
    }
}
