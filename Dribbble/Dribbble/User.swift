//
//  User.swift
//  iShots
//
//  Created by Tope Abayomi on 05/01/2015.
//  Copyright (c) 2015 App Design Vault. All rights reserved.
//

import Foundation


class User {
    
    var userId : Int!
    var avatarUrl : String!
    var name : String!
    var location : String!
    var followingCount : Int!
    var followersCount : Int!
    var shotsCount : Int!
    
    var shotsUrl : String!
    var followingUrl : String!
    
    var avatarData : NSData?
    
    init(data : NSDictionary){
    
        self.userId = data["id"] as! Int
        self.name = Helper.getStringFromJSON(data, key: "name")
        self.avatarUrl = Helper.getStringFromJSON(data, key: "avatar_url")
        
        self.location = Helper.getStringFromJSON(data, key: "location")
        self.followingCount = data["followings_count"] as! Int
        self.followersCount = data["followers_count"] as! Int
        self.shotsCount = data["shots_count"] as! Int
        
        self.shotsUrl = Helper.getStringFromJSON(data, key: "shots_url")
        self.followingUrl = Helper.getStringFromJSON(data, key: "following_url")
    }
    

}