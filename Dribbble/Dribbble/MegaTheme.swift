//
//  MegaTheme.swift
//  Dribbble
//
//  Created by Paulo Henrique Leite on 16/11/15.
//  Copyright © 2015 Paulo Henrique Leite. All rights reserved.
//


import Foundation
import UIKit

class MegaTheme {
    
    class var fontName : String {
        return "Avenir-Book"
    }
    
    class var boldFontName : String {
        return "Avenir-Black"
    }
    
    class var semiBoldFontName : String {
        return "Avenir-Heavy"
    }
    
    class var lighterFontName : String {
        return "Avenir-Light"
    }
    
    class var darkColor : UIColor {
        return UIColor.blackColor()
    }
    
    class var lightColor : UIColor {
        return UIColor(white: 0.6, alpha: 1.0)
    }
}
