//
//  ShotZoomController.swift
//  Dribbble
//
//  Created by Paulo Henrique Leite on 16/11/15.
//  Copyright © 2015 Paulo Henrique Leite. All rights reserved.
//


import Foundation
import UIKit

class ShotZoomController : UIViewController {
    
    @IBOutlet var scrollView : UIScrollView!
    @IBOutlet var shotImageView : UIImageView!
    @IBOutlet var descriptionLabel : UILabel!
    
    var shot : Shot!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        descriptionLabel.textColor = UIColor.whiteColor()
        descriptionLabel.text = shot.description
        
        if let imageData = shot.imageData {
            shotImageView.image = UIImage(data: imageData)
        }
        
        scrollView.contentSize = shotImageView.frame.size
    }
}